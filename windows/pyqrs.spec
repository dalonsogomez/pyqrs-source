# -*- mode: python ; coding: utf-8 -*-

#from kivy_deps import sdl2, glew
#from kivy.tools.packaging.pyinstaller_hooks import get_deps_minimal, hookspath, runtime_hooks

block_cipher = None

a = Analysis(
    ['pyqrs.py'],
    binaries=[('libpng16-16.dll','.'),('zlib1.dll','.')],
    datas=[
          ('cursor_blue_transparant.png','.'),
          ('cursor_red_transparant.png','.'),
          ('pyqrs.kv','.'),
          ('PyQRS.ico','.')
          ],
    hiddenimports=[],
    hookspath=[], # hookspath(),
    runtime_hooks=[],
    excludes=['numpy'],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False
#    , **get_deps_minimal(video=None, audio=None, camera=None, spelling=None, window=True)
    )

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='PyQRS',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=False,
    runtime_tmpdir=None,
    icon='PyQRS.ico',
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    )
    
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    name='PyQRS',
    )
