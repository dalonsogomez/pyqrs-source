'''
This module contains a class 'Extended' which generates tick labels along axes
and some subclasses from the module kivy_garden.graph.
'''

################################################################################
#                               Extended                                       #
################################################################################

'''
The class 'Extended' is based on an algorithm by Talbot, Lin, and Hanrahan.
The source code is written by Justin Talbot and we slightly adapted it from
https://toyplot.readthedocs.io/en/stable/toyplot.locator.htmltoyplot.locator.Extended
The method Extended().extended is used in the graphics code for PyQRS.
'''

# An alpha version of the Talbot, Lin, Hanrahan tick mark generator for matplotlib.
# Described in "An Extension of Wilkinson's Algorithm for Positioning Tick Labels on Axes"
# by Justin Talbot, Sharon Lin, and Pat Hanrahan, InfoVis 2010.

# Implementation by Justin Talbot
# This implementation is in the public domain.
# Report bugs to jtalbot@stanford.edu

# A shortcoming:
# The weights used in the paper were designed for static plots where the extent of
# the tick marks unioned with the extent of the data defines the extent of the plot.
# In a plot where the extent of the plot is defined by the user (e.g. an interactive
# plot supporting panning and zooming), the weights don't work as well. In particular,
# you would want to retune them assuming that the tick labels must be inside
# the provided view range. You probably want higher weighting on simplicity and lower
# on coverage and possibly density. But I haven't experimented in any detail with this.
#
# If you do intend on using this for static plots in matplotlib, you should set
# only_inside to False in the call to Extended.extended. And then you should
# manually set your view extent to include the min and max ticks if they are outside
# the data range. This should produce the same results as the paper.

# import math
from math import pow, floor, ceil, log10

class Extended(object):
    """Generate ticks using "An Extension of Wilkinson's Algorithm for Positioning Tick Labels on Axes" by Talbot, Lin, and Hanrahan.

    Parameters
    ----------
    count: number, optional
      Desired number of ticks.  Note that the algorithm may produce fewer ticks
      than requested.
    steps: sequence of numbers, optional
      Prioritized list of "nice" values to use for generating ticks.
    only_inside: boolean
      If set to `True`, only ticks inside the axis domain will be generated.
    format : string, optional
      Format string used to generate labels from tick locations.
    """
    def __init__(self, count=5, steps=None, weights=None, only_inside=False, format="{0:.{digits}f}"):
        self._count = count
        self._steps = steps if steps is not None else [1, 5, 2, 2.5, 4, 3]
        self._weights = weights if weights is not None else [0.25, 0.2, 0.5, 0.05]
        self._only_inside = only_inside
        self._format = format

    def coverage(self, dmin, dmax, lmin, lmax):
        range_ = dmax - dmin
        return 1 - 0.5 * (pow(dmax - lmax,
                                      2) + pow(dmin - lmin,
                                                       2)) / pow(0.1 * range_,
                                                                         2)

    def coverage_max(self, dmin, dmax, span):
        range_ = dmax - dmin
        if span > range_:
            half = (span - range_) / 2.0
            return 1 - pow(half, 2) / pow(0.1 * range_, 2)
        else:
            return 1

    def density(self, k, m, dmin, dmax, lmin, lmax):
        r = (k - 1.0) / (lmax - lmin)
        rt = (m - 1.0) / (max(lmax, dmax) - min(lmin, dmin))
        return 2 - max(r / rt, rt / r)

    def density_max(self, k, m):
        if k >= m:
            return 2 - (k - 1.0) / (m - 1.0)
        else:
            return 1

    def simplicity(self, q, Q, j, lmin, lmax, lstep):
        eps = 1e-10
        n = len(Q)
        i = Q.index(q) + 1
        v = 1 if ((lmin % lstep < eps or (lstep - lmin % lstep)
                   < eps) and lmin <= 0 and lmax >= 0) else 0
        return (n - i) / (n - 1.0) + v - j

    def simplicity_max(self, q, Q, j):
        n = len(Q)
        i = Q.index(q) + 1
        v = 1
        return (n - i) / (n - 1.0) + v - j

    def legibility(self, lmin, lmax, lstep):
        return 1

    def legibility_max(self, lmin, lmax, lstep):
        return 1  # pragma: no cover

    def extended (self, dmin, dmax, m, Q=[1,5,2,2.5,4,3], only_inside=False, w=[0.25,0.2,0.5,0.05]): # (original)
        best_score = -2.0

        j = 1.0
        while j < float('infinity'):
            for q in Q:
                sm = self.simplicity_max(q, Q, j)

                if w[0] * sm + w[1] + w[2] + w[3] < best_score:
                    j = float('infinity')
                    break

                k = 2.0
                while k < float('infinity'):
                    dm = self.density_max(k, m)

                    if w[0] * sm + w[1] + w[2] * dm + w[3] < best_score:
                        break

                    delta = (dmax - dmin) / (k + 1.0) / j / q
                    z = ceil(log10(delta))

                    while z < float('infinity'):
                        step = j * q * pow(10, z)
                        cm = self.coverage_max(dmin, dmax, step * (k - 1.0))

                        if w[0] * sm + w[1] * cm + \
                                w[2] * dm + w[3] < best_score:
                            break

                        min_start = floor(
                            dmax / step) * j - (k - 1.0) * j
                        max_start = ceil(dmin / step) * j

                        if min_start > max_start:
                            z = z + 1
                            break

                        for start in range(
                                int(min_start),
                                int(max_start) + 1):
                            lmin = start * (step / j)
                            lmax = lmin + step * (k - 1.0)
                            lstep = step

                            s = self.simplicity(q, Q, j, lmin, lmax, lstep)
                            c = self.coverage(dmin, dmax, lmin, lmax)
                            d = self.density(k, m, dmin, dmax, lmin, lmax)
                            l = self.legibility(lmin, lmax, lstep)

                            score = w[0] * s + w[1] * \
                                c + w[2] * d + w[3] * l

                            if score > best_score and (not only_inside or (lmin >= dmin and lmax <= dmax)):
                                best_score = score
                                best = (lmin, lmax, lstep, q, k)
                        z = z + 1
                    k = k + 1
            j = j + 1
        return best


################################################################################
#                               PyQRS_graphs                                   #
################################################################################

'''
Graphics code for PyQRS is taken from kivy_garden.graph from which a few
classes were subclassed and one method was added.
The module kivy_garden.graph is maintained on github:
https://github.com/kivy-garden/graph
'''
from kivy import metrics

def _new_get_ticks(self, major, minor, log, s_min, s_max):
    if major and s_max > s_min:
        # instead of the original label generator,
        # the next 4 lines are used (see Talbot's Extended class above, SK)
        (lmin, lmax, lstep, q, k) = Extended().extended(s_min, s_max, 6,
                                        only_inside=True, w=[0.35,0.2,0.4,0.05])
        k = int(k)
        points_major = [lmin + i * lstep for i in range(k)]
        points_minor = []
    else:
        points_major = []
        points_minor = []
    return points_major, points_minor

def _new_update_ticks(self, size):
    # re-compute the positions of the bounding rectangle
    mesh = self._mesh_rect
    vert = mesh.vertices
    vert[0:18] = [0 for k in range(18)]
    mesh.vertices = vert
    # re-compute the positions of the x/y axis ticks
    mesh = self._mesh_ticks
    vert = mesh.vertices
    start = 0
    xpoints = self._ticks_majorx
    ypoints = self._ticks_majory
    xpoints2 = self._ticks_minorx
    ypoints2 = self._ticks_minory
    ylog = self.ylog
    xlog = self.xlog
    xmin = self.xmin
    xmax = self.xmax
    ymin = self.ymin
    ymax = self.ymax
    if len(xpoints):
#       top = size[3] if self.x_grid else metrics.dp(12) + size[1] # original line
#       In PyQRS tick should point downwards, SK 
        top = size[3] if self.x_grid else size[1] - metrics.dp(6)
        ratio = (size[2] - size[0]) / float(xmax - xmin)
        for k in range(start, len(xpoints) + start):
            vert[k * 8] = size[0] + (xpoints[k - start] - xmin) * ratio
            vert[k * 8 + 1] = size[1]
            vert[k * 8 + 4] = vert[k * 8]
            vert[k * 8 + 5] = top
        start += len(xpoints)
    if len(xpoints2):
        top = metrics.dp(8) + size[1]
        ratio = (size[2] - size[0]) / float(xmax - xmin)
        for k in range(start, len(xpoints2) + start):
            vert[k * 8] = size[0] + (xpoints2[k - start] - xmin) * ratio
            vert[k * 8 + 1] = size[1]
            vert[k * 8 + 4] = vert[k * 8]
            vert[k * 8 + 5] = top
        start += len(xpoints2)
    if len(ypoints):
        top = size[2] if self.y_grid else metrics.dp(12) + size[0]
        ratio = (size[3] - size[1]) / float(ymax - ymin)
        for k in range(start, len(ypoints) + start):
            vert[k * 8 + 1] = size[1] + (ypoints[k - start] - ymin) * ratio
            vert[k * 8 + 5] = vert[k * 8 + 1]
            vert[k * 8] = size[0]
            vert[k * 8 + 4] = top
        start += len(ypoints)
    if len(ypoints2):
        top = metrics.dp(8) + size[0]
        ratio = (size[3] - size[1]) / float(ymax - ymin)
        for k in range(start, len(ypoints2) + start):
            vert[k * 8 + 1] = size[1] + (ypoints2[k - start] - ymin) * ratio
            vert[k * 8 + 5] = vert[k * 8 + 1]
            vert[k * 8] = size[0]
            vert[k * 8 + 4] = top
    mesh.vertices = vert

def to_window(self, xd, yd):  # added for PyQRS by SK
    '''Convert data coords to window coords.

    :Parameters:
        `x, y`:
            The coordinates to convert (in data coords).
    '''
    norm_x = float(xd - self.xmin) / (self.xmax - self.xmin)
    norm_y = float(yd - self.ymin) / (self.ymax - self.ymin)
    xw = norm_x * self._plot_area.size[0] + self._plot_area.pos[0]
    yw = norm_y * self._plot_area.size[1] + self._plot_area.pos[1]
    return [xw / self.size[0], yw / self.size[1]]
   
    
