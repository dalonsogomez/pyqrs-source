# PyQRS

**(Probabilities, Quantiles and Random Samples)**

For any given probability distribution, PyQRS lets you:  

* calculate probabilities,  
* calculate quantiles (inverse probabilities),  
* draw random samples,  
* calculate a missing (non-integer) parameter value.

Included are:  

* 10 discrete distributions, including the null distributions of the Wilcoxon/Mann-Whitney test statistics,  

and   

* 22 continuous distributions, including the central and non-central t-, chi-square- and F-distributions.

PyQRS was originally developed as a tool for students studying statistics.
It was meant to replace traditional probability tables and at the same time it should enhance the students' understanding of probability concepts. 
Therefore a graphical user interface was designed which shows all values in their natural position with respect to the probability (density/mass) function and with respect to the cumulative distribution function.

## Showcase

The user interface features four views/tabs.

### 1. Specification

| Starting screen: standard normal distribution                    | Specified: binomial (20, 0.3) distribution                       |
|:----------------------------------------------------------------:|:----------------------------------------------------------------:|
| ![Starting screen](metadata/en-US/images/phoneScreenshots/1.png) | ![Binomial screen](metadata/en-US/images/phoneScreenshots/2.png) |

<br>

### 2. Probability density/mass function

| Standard normal probability density function                                      | Binomial (20, 0.3) probability mass function                                   |
|:---------------------------------------------------------------------------------:|:------------------------------------------------------------------------------:|
| ![Standard normal density function](metadata/en-US/images/phoneScreenshots/3.png) | ![Binomial probability function](metadata/en-US/images/phoneScreenshots/4.png) |

<br>

### 3. Cumulative distribution function

| Standard normal cumulative distribution function                                                  | Binomial (20,0.3) cumulative distribution function                                         |
|:-------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------:|
| ![Standard normal cumulative distribution function](metadata/en-US/images/phoneScreenshots/5.png) | ![Binomial cumulative distribution function](metadata/en-US/images/phoneScreenshots/6.png) |

<br>

### 4. Random samples

| Random sample from a standard normal distribution                                                | Random sample from a binomial (20,0.3) distribution                                       |
|:------------------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------:|
| ![Random sample from standard normal distribution](metadata/en-US/images/phoneScreenshots/7.png) | ![Random sample from binomial distribution](metadata/en-US/images/phoneScreenshots/8.png) |

<br>

### Fit a parameter value

Additionally, you can use PyQRS to find an unknown parameter value, given the 
distribution name, given the remaining parameter value(s), given an $x$-value 
and given the cumulative probability at $x$. For these and other details see the
[manual](https://pyqrs.eu/sk/pyqrs/using_pyqrs).

## Included distributions

| Discrete             | Continuous                   | Continuous             |
|:-------------------- |:---------------------------- |:---------------------- |
| binomial             | normal (gaussian)            | inverse gaussian       |
| Bernoulli            | t                            | non-central t          |
| hypergeometric       | chi-square                   | non-central chi-square |
| discrete-uniform     | F                            | non-central F          |
| negative-binomial    | beta                         | non-central beta       |
| geometric            | gamma                        | Cauchy                 |
| Poisson              | exponential                  | Gumbel                 |
| Mann-Whitney         | double-exponential (Laplace) | Pareto                 |
| Wilcoxon rank-sum    | logistic                     | Weibull                |
| Wilcoxon signed rank | log-normal                   | triangular             |
|                      | folded-normal                | uniform                |

<br>

## Download and install

PyQRS was written in Python with the help of the Kivy framework.  
If you have Python3  installed on your computer, it is not difficult to run PyQRS using the source code. This is the recommended procedure for Linux and Mac users:  

1. After downloading and unzipping the [source code](https://gitlab.com/pyqrs/pyqrs-source/-/blob/main/for_users/PyQRS-source.zip) in a folder, open a terminal and navigate to this folder.
2. Install the necessary dependencies; e.g. Debian and Ubuntu users may issue the following command:  
   `sudo apt install python3-pip xclip xsel`
3. Install the necessary python modules:  
   `python3 -m pip install --upgrade pip kivy[base] kivy_garden.graph`
4. Run PyQRS:  
   `python3 pyqrs.py`

Binaries are available for the [Android](#android) and [Windows](#windows) platform.

### Android

Versions of PyQRS are available for the following four processor architectures:

| <br>                                      | ARM-based                                                                                                                        | x86-based                                                                                                                      |
| ----------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| **64 bits** &nbsp; &nbsp; &nbsp;<br><br>  | [PyQRS-Android-arm64-v8a.apk][v8a] <br> 15.0 MB<br> last update 2023.04.24                                                       | [PyQRS-Android-x86_64.apk][x86_64] <br> also referred to as x64 and AMD64 instruction set <br> 15.8 MB, last update 2023.04.24 |
| **32 bits** &nbsp; &nbsp; &nbsp;<br> <br> | [PyQRS-Android-armeabi-v7a.apk][v7a] &nbsp; &nbsp; &nbsp;<br> for usually older ARM-devices <br> 14.0 MB, last update 2023.04.28 | [PyQRS-Android-x86.apk][x86] <br> 15.8 MB <br> last update 2023.04.24                                                          |

[v8a]: https://gitlab.com/pyqrs/pyqrs-source/-/blob/main/for_users/PyQRS-Android-arm64-v8a.apk
[v7a]: https://gitlab.com/pyqrs/pyqrs-source/-/blob/main/for_users/PyQRS-Android-armeabi-v7a.apk
[x86_64]: https://gitlab.com/pyqrs/pyqrs-source/-/blob/main/for_users/PyQRS-Android-x86_64.apk
[x86]: https://gitlab.com/pyqrs/pyqrs-source/-/blob/main/for_users/PyQRS-Android-x86.apk

If you have a 64 bits ARM device, you can also run the 32 bits ARM version on it.  
If you are having difficulties to determine which zip file you need, you can (temporarily) install an app that shows you the architecture. _Inware_ is such an app. If you install it and go to the 'System' tab, you find the answer under 'Instruction Sets'.

Then

1. Download appropriate apk file on your Android device[^remark],
2. The option 'Unknown sources' should be enabled (under Settings/Security). 
3. Then click/tap on the filename with extension `.apk`; you will be asked whether you want to install the package.
4. Confirm; the app does not need access to personal data or peripherals. In order to show Wikipedia pages or the PyQRS manual, you will need a working internet connection.

[^remark]:  Some browsers on Android download a file with a `.bin` extension. In that case try to use another browser.

### Windows (64 bits) binaries

1. Download [PyQRS-windows.zip][windows_x86] (14.2 MB, last update 2023.02.27).
2. Unzip the downloaded file.
3. In the newly created directory find the executable file PyQRS.exe.
4. (Optional) Create a shortcut for this file on your desktop.
5. Execute the file.

[windows_x86]: https://gitlab.com/pyqrs/pyqrs-source/-/blob/main/for_users/PyQRS-windows.zip

All mentioned packages are also available through [the GitLab project page](https://gitlab.com/pyqrs/pyqrs-source/-/releases).


