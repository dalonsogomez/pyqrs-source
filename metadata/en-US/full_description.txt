# PyQRS

PyQRS (Probabilities, Quantiles and Random Samples) lets you: 

* calculate probabilities,  
* calculate quantiles (inverse probabilities),  
* draw random samples,  
* calculate a missing parameter value

for a given distribution.

Available are:  

* 10 discrete distributions, including the null distributions of the Wilcoxon/Mann-Whitney test statistics,  
and   
* 22 continuous distributions, including the central and non-central t-, chi-square- and F-distributions.

PyQRS was originally developed as a tool for students studying statistics.
It was meant to replace traditional probability tables and at the same time it should enhance the students' understanding of probability concepts. 
Therefore a graphical user interface was designed that shows all values in their natural position with respect to the probability (density/mass) function and with respect to the cumulative distribution function.

